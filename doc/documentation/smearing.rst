.. _smearing:

Occupation number smearing
==========================

.. module:: gpaw.occupations

.. seealso:: :ref:`manual_occ`

.. autofunction:: create_occupation_number_object
.. autofunction:: fermi_dirac
.. autofunction:: marzari_vanderbilt
.. autofunction:: methfessel_paxton
.. autoclass:: OccupationNumbers
   :members:
.. autoclass:: FixedOccupationNumbers
.. autoclass:: ParallelLayout
.. autofunction:: occupation_numbers
